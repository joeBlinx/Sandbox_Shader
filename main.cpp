#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include "glish3/glish3.hpp"
#include <filesystem>
#include <algorithm>

struct CheckTimeStamp
{
    std::filesystem::path _path;
    mutable std::filesystem::file_time_type _last_write;

    CheckTimeStamp(std::filesystem::path path) : _path(std::move(path)), _last_write(std::filesystem::last_write_time(_path))
    {
    }
    bool checkTimeStamp() const
    {
        auto last_write_time_temp = std::filesystem::last_write_time(_path);
        bool check = last_write_time_temp != _last_write;
        if (check)
        {
            _last_write = last_write_time_temp;
        }
        return check;
    }
};

static auto const shader_path = std::filesystem::path("assets/shader");
static auto const vertex_shader_path = shader_path / std::filesystem::path("point.vert");
static auto const geometry_shader_path = shader_path / std::filesystem::path("point.geom");
static auto const fragment_shader_path = shader_path / std::filesystem::path("point.frag");

void init_ogl()
{
    glewExperimental = GL_TRUE;
    auto err = glewInit();
    if (err != GLEW_OK)
    {
        std::cerr << glewGetErrorString(err) << std::endl;
        throw std::runtime_error("error while initialize glew");
    }
    glGetError();
    glClearColor(0.5, 0.5, 0.5, 1);
    glGetError();
    glEnable(GL_BLEND);
    glGetError();
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glGetError();
}
void reload_shaders(glish3::ProgramGL& program){
    glish3::Shader vertex = glish3::Shader::createShaderFromFile(GL_VERTEX_SHADER, vertex_shader_path.c_str());
    glish3::Shader geom = glish3::Shader::createShaderFromFile(GL_GEOMETRY_SHADER, geometry_shader_path.c_str());
    glish3::Shader fragment = glish3::Shader::createShaderFromFile(GL_FRAGMENT_SHADER, fragment_shader_path.c_str());
    program = glish3::ProgramGL(vertex, geom, fragment);
    program.use();
}
void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ENTER && action == GLFW_PRESS)
    {
        auto &program = *reinterpret_cast<glish3::ProgramGL *>(glfwGetWindowUserPointer(window));

        reload_shaders(program);
    }
}
bool is_a_file_modified(std::vector<CheckTimeStamp> const& timeStamps){
    return std::any_of(begin(timeStamps), end(timeStamps), [](const auto& timeStamp){
        return timeStamp.checkTimeStamp();
    });
}
int main()
{
    GLFWwindow *window;

    /* Initialize the library */
    if (!glfwInit())
        return -1;

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(600, 600, "Hello World", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);
    glfwSetKeyCallback(window, key_callback);
    init_ogl();
    float points[] = {
        -0.5f, 0.5f,
        0.5f, 0.5f,
        -0.5f, -0.5f,
        0.5f, -0.5f};
    glish3::init("logfiles", "Sandbox Shader");
    glish3::Vao vao;
    vao.addVbo(glish3::Vbo(
        GL_ARRAY_BUFFER, points, glish3::vbo_settings{2}));

    std::vector<CheckTimeStamp> timeStamps {
        {vertex_shader_path},{geometry_shader_path},{fragment_shader_path}
    };

    glish3::Shader vertex = glish3::Shader::createShaderFromFile(GL_VERTEX_SHADER, vertex_shader_path.c_str());
    glish3::Shader geom = glish3::Shader::createShaderFromFile(GL_GEOMETRY_SHADER, geometry_shader_path.c_str());
    glish3::Shader fragment = glish3::Shader::createShaderFromFile(GL_FRAGMENT_SHADER, fragment_shader_path.c_str());
    glish3::ProgramGL prog(vertex, geom, fragment);
    prog.use();

    glfwSetWindowUserPointer(window, &prog);
    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window))
    {
        /* Render here */
        glClear(GL_COLOR_BUFFER_BIT);
        if(is_a_file_modified(timeStamps)){
            reload_shaders(prog);
        }
        glishDrawArrays(GL_POINTS, 0, 4);
        /* Swap front and back buffers */
        glfwSwapBuffers(window);

        /* Poll for and process events */
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
