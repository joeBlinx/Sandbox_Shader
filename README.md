# BUILD
you need to have conan install
```shell
python -m pip install conan==1.32
```
You may also need to install `xorg-dev` in order to use GLFW
```shell
sudo apt install -y xorg-dev
```