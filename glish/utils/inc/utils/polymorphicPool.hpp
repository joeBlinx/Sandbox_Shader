//
// Created by Stiven on 14-Apr-18.
//

#ifndef UTILS_POLYMORPHICPOOL_HPP
#define UTILS_POLYMORPHICPOOL_HPP

#include <iostream>
#include <list>

/* Problem with the struct info, use of static variable, force you to use only one instance
of PoolPoly<N, Ts...>*/

template<unsigned N, class Base, class ...Derived>
class PoolPoly{
	template<class IT, class Type>
	struct info{

		using iterator = IT;
		inline static iterator begin;
		inline static iterator end;
		inline static bool empty = false;

		static iterator get(){
			if(begin == end){
				empty = true;
			}
			return begin++;
		}
		static bool isEmpty(){
			return empty;
		}
	};

	std::list<Base*> pool;
	using List = std::list<Base *>;

	static constexpr unsigned numberDerivedClass = sizeof...(Derived);
	using iterator = typename std::list<Base*>::iterator;

	template<class T, class ...Ts>
	void fill(iterator it){
		info<iterator, T>::begin = it;
		auto a = new T[N];
		for (unsigned i = 0; i < N ; i++){
			*it = a++;
			it++;

		}
		auto next = it;
		next--;
		info<iterator, T>::end = next;
		if constexpr (sizeof...(Ts) > 0){
			fill<Ts...>(it);
		}
	}

	template<class T, class Test, class ...Other>
	constexpr void check(){
		if constexpr (!std::is_same<T, Test>::value
					  && sizeof...(Other) != 0){
			check<T, Other...>();
		}else{
			static_assert(std::is_same<T, Test>::value,"type is not allowed");
		}

	}
public:

	PoolPoly():pool(N*numberDerivedClass){

		fill<Derived...>(pool.begin());
	}

	template<class T>
	Base* get(){
		check<T, Derived...>();
		using infoType = info<iterator, T>;
		if(infoType::isEmpty()){
			std::cerr << "no more element of type "<<  typeid(T).name()<< " left " << std::endl;
			return nullptr;
		}
		iterator a = infoType::get();
		Base * returnValue = *a;
		pool.erase(a);
		return returnValue;

	}

	~PoolPoly(){
		for(auto & a: pool){
			delete a;
		}
	}
};
#endif //UTILS_POLYMORPHICPOOL_HPP
